package lt.julius.library.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lt.julius.library.library.entity.Book;
import lt.julius.library.library.service.LibraryService;

@RestController
@RequestMapping("/library")
public class LibraryController {

	@Autowired
	private LibraryService libraryService;

	@GetMapping("/books")
	public Book getBook(@RequestParam String bookId) {
		return libraryService.getBook(bookId);
	}

	@PostMapping("/books")
	public void getBook(@RequestBody Book book) {
		libraryService.createBook(book);
	}

}
