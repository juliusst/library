package lt.julius.library.library.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import lt.julius.library.library.entity.Book;

@Service
public class LibraryService {
	
	private List<Book> books;
	
	public LibraryService() {
		books = new ArrayList<Book>();
		books.add(new Book("1", "Demo Book"));
		books.add(new Book("100", "Test Book"));
	}

	public Book getBook(String bookId) {
		return books.get(Integer.parseInt(bookId));
	}

	public void createBook(Book book) {
		books.add(book);
	}

}
